package com.example.geoquiz;

import static androidx.constraintlayout.motion.utils.Oscillator.TAG;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import org.w3c.dom.Text;

import java.text.DecimalFormat;

public class MainActivity extends Activity {
    private Boolean[] TorF = {
            true, true, true, true, true, true,
    };
    private int num_True = 0;
    private int num_false = 0;
    private int num = 0;
    private double grade = 0;


    private Button mCheatButton;
    private boolean mIsCheater;
    private TextView mResult_num;
    private Button mTrueButton;
    private Button mFalseButton;
    private ImageButton mPrevButton;
    private ImageButton mNextButton;
    private TextView mQuestionTextView;
    private static String KEY_INDEX = "index";
    private static final int REQUEST_CODE_CHEAT = 0;

    private Boolean change = false;


    private Question[] mQuestionBank = new Question[]{
            new Question(R.string.question_australia, true),
            new Question(R.string.question_oceans, true),
            new Question(R.string.question_mideast, false),
            new Question(R.string.question_africa, false),
            new Question(R.string.question_americas, true),
            new Question(R.string.question_asia, true),
    };

    private int mCurrentIndex = 0;
    private Button mGrade;
    private Button show_grade;
    private ViewGroup mShow;

    @Override
    public void onSaveInstanceState(Bundle saveInstanceState) {
        super.onSaveInstanceState(saveInstanceState);
        saveInstanceState.putInt(KEY_INDEX, mCurrentIndex);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        if (requestCode == REQUEST_CODE_CHEAT) {
            if (data == null) {
                return;
            }
            mIsCheater = activity_cheat.wasAnswerShown(data);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            mCurrentIndex = savedInstanceState.getInt(KEY_INDEX);
        }
        setContentView(R.layout.activity_main);

        show_grade = (Button) findViewById(R.id.show_grade);
        mGrade = findViewById(R.id.show_grade);
        mShow = findViewById(R.id.mShow);


        mQuestionTextView = (TextView) findViewById(R.id.question_text_view);
        mTrueButton = (Button) findViewById(R.id.true_button);
        mFalseButton = (Button) findViewById(R.id.false_button);

        mNextButton = (ImageButton) findViewById(R.id.next_button);
        mPrevButton = (ImageButton) findViewById(R.id.prev_button);
        mResult_num = (TextView) findViewById(R.id.result_num);

        mCheatButton = (Button) findViewById(R.id.cheat_button);


        mTrueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer(true);
            }
        });


        mFalseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer(false);
            }
        });
        mResult_num.setText("1");


        mNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                mCurrentIndex = (mCurrentIndex + 1) % mQuestionBank.length;
                if (mCurrentIndex < mQuestionBank.length - 1) {
                    mCurrentIndex++;
                    mResult_num.setText(mCurrentIndex + 1 + "");
                    if (mCurrentIndex == 5) {
//                        Toast.makeText(MainActivity.this,num_True+"",Toast.LENGTH_SHORT).show();

                        mGrade.setVisibility(View.VISIBLE);
                        Showing();
                    }
                } else {
                    Toast.makeText(MainActivity.this, "已是最后一题", Toast.LENGTH_SHORT).show();
                }
                mIsCheater = false;
                updateQuestion();
            }

        });


        mPrevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCurrentIndex > 0) {
                    mCurrentIndex--;
                    mResult_num.setText(mCurrentIndex + 1 + "");
                    mGrade.setVisibility(View.INVISIBLE);
                    mShow.setVisibility(View.INVISIBLE);
                } else
//                    mCurrentIndex = mQuestionBank.length - 1;
                    Toast.makeText(MainActivity.this, "已经是第一题", Toast.LENGTH_SHORT).show();
                updateQuestion();
            }
        });


        mCheatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent(MainActivity.this, activity_cheat.class);
                boolean answerIsTrue = mQuestionBank[mCurrentIndex].isAnswerTrue();
                Intent intent = activity_cheat.newIntent(MainActivity.this, answerIsTrue);
                startActivityForResult(intent, REQUEST_CODE_CHEAT);
            }
        });

        updateQuestion();
    }


    private void updateQuestion() {
        int question = mQuestionBank[mCurrentIndex].getTextResId();
        mQuestionTextView.setText(question);
    }

    private void checkAnswer(boolean userPressedTrue) {
        boolean answerIsTrue = mQuestionBank[mCurrentIndex].isAnswerTrue();

        int messageResId = 0;
        if (mIsCheater) {
            messageResId = R.string.judgement_toast;
        } else {
            if (userPressedTrue == answerIsTrue) {
                Sum_Grade(true);
//                Show_Grade();
                messageResId = R.string.correct_toast;
            } else {
                Sum_Grade(false);
//                Show_Grade();
                messageResId = R.string.incorrect_toast;
            }

            Toast.makeText(this, messageResId, Toast.LENGTH_SHORT)
                    .show();
        }


    }

    public void Show_Grade() {
        DecimalFormat dc = new DecimalFormat("#%");
        TextView result_Num = findViewById(R.id.final_result_Num);
        TextView result_True = findViewById(R.id.final_result_True);
        TextView result_False = findViewById(R.id.final_result_False);
        TextView Correct = findViewById(R.id.correct);
        result_Num.setText("" + num);
        result_True.setText("" + num_True);
        result_False.setText("" + num_false);

        Correct.setText("" + dc.format((double) num_True / num));
    }


    void Sum_Grade(boolean a) {
        if (a == true) {
            if (mCurrentIndex < mQuestionBank.length) {
                if (TorF[mCurrentIndex] == true) {
                    num++;
                    num_True++;
                    TorF[mCurrentIndex] = false;
                }
            }
        } else {
            if (mCurrentIndex < mQuestionBank.length) {
                if (TorF[mCurrentIndex] == true) {
                    num++;
                    num_false++;
                    TorF[mCurrentIndex] = false;
                }
            }
        }
    }

    void Showing() {
        show_grade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (num == 0) {
                    Toast.makeText(MainActivity.this, "请先答题！", Toast.LENGTH_SHORT).show();
                } else {
                    mShow.setVisibility(View.VISIBLE);
                    Show_Grade();
                }
            }
        });
    }

}
